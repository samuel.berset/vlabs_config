ip addr add 192.168.1.10/24 dev eth1
route add default gw 192.168.1.1

ip -6 addr add 2001:1::10/64 dev eth1
ip -6 route replace default via 2001:1::1 dev eth1
